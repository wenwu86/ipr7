import numpy as np
import cv2
import gradio as gr
import cv2

def get_dominant_colors(image, k=5):
    # 转换为RGB格式（如果尚未转换）
    if len(image.shape) == 3 and image.shape[2] == 3:
        img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    else:
        img = image
    
    # 将图像数据转换为二维数组
    pixel_values = img.reshape((-1, 3))
    pixel_values = np.float32(pixel_values)
    
    # 定义KMeans聚类的停止条件
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    
    # 运行KMeans聚类
    _, labels, centers = cv2.kmeans(pixel_values, k, None, criteria, 10, cv2.KMEANS_PP_CENTERS)
    
    # 将聚类中心转换为整数类型
    centers = np.uint8(centers)
    
    # 获取主色调的十六进制表示
    hex_codes = ['#{:02x}{:02x}{:02x}'.format(center[0], center[1], center[2]) for center in centers]
    
    # 根据聚类结果生成分割后的图像
    segmented_image = centers[labels.flatten()]
    segmented_image = segmented_image.reshape(img.shape)
    
    return centers, hex_codes, segmented_image

def process_image(img):
    # 读取上传的图像并转换颜色通道
    image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
    # 获取主色调和十六进制码
    colors, hex_codes, segmented_image = get_dominant_colors(image)
    
    # 生成颜色展示的HTML代码
    color_blocks = ''
    for color in colors:
        color_hex = '#{:02x}{:02x}{:02x}'.format(int(color[0]), int(color[1]), int(color[2]))
        color_blocks += f'<div style="width:60px;height:60px;background-color:{color_hex};display:inline-block;margin:5px;"></div>'
    
    # 将十六进制码合并为字符串
    hex_codes_str = '\n'.join(hex_codes)
    
    return color_blocks, hex_codes_str, segmented_image

with gr.Blocks() as demo:
    with gr.Row():
        with gr.Column():
            image_input = gr.Image(label="上传图片", type="numpy")
        with gr.Column():
            color_output = gr.HTML(label="主色调")
            hex_output = gr.Textbox(label="色彩十六进制码")
            segmented_output = gr.Image(label="分割后的图像")
    image_input.change(process_image, inputs=image_input, outputs=[color_output, hex_output, segmented_output])

demo.launch()